FROM alpine

LABEL maintainer="Vasco Brito"

COPY myapp/ /opt/myapp

WORKDIR /opt/myapp

CMD ["cat","cv-equipa2.txt"]
